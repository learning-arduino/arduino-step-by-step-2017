#include <Arduino.h>
#include "User_Setup.h"

#define METER_PIN A0

void setup() {
    Serial.begin(9600);
}

void loop() {
    Serial.println(analogRead(METER_PIN));
    delay(500);
}
