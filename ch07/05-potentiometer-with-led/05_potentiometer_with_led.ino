#include <Arduino.h>
#include "User_Setup.h"

#define METER_PIN A0
#define LED_PIN 6

void setup() {
    pinMode(LED_PIN, OUTPUT);
}

void loop() {
    int potValue = analogRead(METER_PIN);               // possible values 0..1023
    int mappedValue = map(potValue, 0, 1023, 0, 255);   // need to map values
    analogWrite(LED_PIN, mappedValue);                 // possible values 0..255
}
