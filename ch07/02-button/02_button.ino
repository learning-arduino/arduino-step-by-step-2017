#include <Arduino.h>
#include "User_Setup.h"

#define BUTTON_PIN 2

void setup() {
    pinMode(BUTTON_PIN, INPUT);
    Serial.begin(9600);
}

void loop() {
    int buttonState = digitalRead(BUTTON_PIN);
    if (buttonState == HIGH) {
        Serial.println("HIGH");
    } else {
        Serial.println("LOW");
    }
    delay(500);
}
