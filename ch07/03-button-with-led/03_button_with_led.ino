#include <Arduino.h>
#include "User_Setup.h"

#define LED_PIN 2
#define BUTTON_PIN 4

void setup() {
    pinMode(LED_PIN, OUTPUT);
    pinMode(BUTTON_PIN, INPUT);
    Serial.begin(9600);
}

void loop() {
    int buttonState = digitalRead(BUTTON_PIN);
    if (buttonState == HIGH) {
        Serial.println("HIGH");
    } else {
        Serial.println("LOW");
    }
    digitalWrite(LED_PIN, buttonState);
    delay(500);
}
