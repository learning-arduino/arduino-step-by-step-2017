#include <Arduino.h>
#include "User_Setup.h"

#define RED_PIN 3
#define GREEN_PIN 5
#define BLUE_PIN 6

void blinkColor(int pin) {
    digitalWrite(pin, HIGH);
    delay(1000);
    digitalWrite(pin, LOW);
} 

void setup() {
    pinMode(RED_PIN, OUTPUT);
    pinMode(GREEN_PIN, OUTPUT);
    pinMode(BLUE_PIN, OUTPUT);
}

void loop() {
    blinkColor(RED_PIN);
    blinkColor(GREEN_PIN);
    blinkColor(BLUE_PIN);
}
