#include <Arduino.h>
#include "User_Setup.h"

#define LED_PIN 8
#define LONG_INTERVAL 500
#define SHORT_INTERVAL 300

void blink(int interval) {
    for (int i = 0; i < 3; i++) {
        digitalWrite(LED_PIN, HIGH);
        delay(interval);
        digitalWrite(LED_PIN, LOW);
        delay(interval);
    }
}

void setup() {
    pinMode(LED_PIN, OUTPUT);
}

void loop() {
    blink(SHORT_INTERVAL);
    blink(LONG_INTERVAL);
    blink(SHORT_INTERVAL);
    delay(1000);
}
