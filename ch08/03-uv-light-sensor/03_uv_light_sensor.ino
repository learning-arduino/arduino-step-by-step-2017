#include <Arduino.h>
#include "User_Setup.h"

void setup() {
    Serial.begin(9600);
}

void loop() {
    int sensorValue = analogRead(A0);

    double voltage = sensorValue * (5.0 / 1023.0);

    Serial.print(sensorValue);
    Serial.print(",");
    Serial.println(voltage / 0.1); // UV index value
    delay(100);
}
